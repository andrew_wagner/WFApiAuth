﻿namespace MailRuAuthDemo
{
    public interface IAuth
    {
        void Login(string login, string password);
        void GetData(string url);
        void Logout();
    }
}
