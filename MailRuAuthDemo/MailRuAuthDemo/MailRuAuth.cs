﻿using System;
using System.Collections.Generic;
using xNet;

namespace MailRuAuthDemo
{
    public sealed class MailRuAuth : IAuth
    {
        HttpRequest request = new HttpRequest();
        public void Login(string login, string password)
        {
            request.Cookies = new CookieDictionary();
            request.AddHeader(HttpHeader.Accept, "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
            request.AddHeader(HttpHeader.Referer, "https://wf.mail.ru/");
            request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36";
            request.Get("https://wf.mail.ru/").ToString();
            RequestParams pairs = new RequestParams();
            pairs.Add(new KeyValuePair<string, string>("Page", "https://wf.mail.ru/"));
            pairs.Add(new KeyValuePair<string, string>("FakeAuthPage", "https://wf.mail.ru/auth"));
            pairs.Add(new KeyValuePair<string, string>("Login", login));
            pairs.Add(new KeyValuePair<string, string>("Password", password));
            request.Post("https://auth.mail.ru/cgi-bin/auth", pairs);
        }

        public void GetData(string url)
        {
            Console.WriteLine(request.Get(url).ToString());
        }

        public void Logout()
        {
            request.Get("https://wf.mail.ru/dynamic/auth/?plogout=1");
        }


    }
}
